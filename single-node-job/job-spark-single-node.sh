#!/bin/bash
#SBATCH -J spark-test                   # Identificação do job
#SBATCH -o %j.out                       # Nome do arquivo de saída padrão (%j = ID do JOB)
#SBATCH -t 12:00:00                     # Tempo de execução (hh:mm:ss)
#SBATCH --mail-user={{ coloque seu email aqui, por razões obvias não deixei o meu }}
#SBATCH --mail-type=ALL
#SBATCH --cpus-per-task=10 --mem-per-cpu=7g --ntasks=4

service=cloud-igor                          # Nome do perfil remoto no rclone
remote_in=hpc/input                         # Pasta no serviço remoto para input
remote_out=hpc/output                       # Pasta no serviço remoto para output
sing=spark.sif                              # Nome da imagem singularity
remote_sing=hpc/containers/$sing            # Caminho para a imagem singularity no drive
local_sing=./$sing                          # Local para o container singularity
local_job="/scratch/job.${SLURM_JOB_ID}"    # Pasta temporária local
local_in="${local_job}/input/"              # Pasta local para arquivos de entrada
local_out="${local_job}/output/"            # Pasta local para arquivos de saída
spark_conf_dir="${local_job}/SparkConf"     # Pasta local para configuração do spark
spark_tmp_dir="${local_job}/tmp"            # Pasta local para arquivos temporários do spark (shuffles)
spark_master="spark://$(hostname -f):7077"  # URL do processo master do cluster spark (lembrando que o script do job sempre roda em uma das máquinas alocadas)
spark_executor_cores=10                     # Número de cores utilizados em cada processo worker spawnado
spark_executor_memory=60g                   # Quantidade de memoria alocada para cada processo worker spawnado
spark_executor_instances=3                  # Quantidade de processos workers spawnados
spark_driver_cores=10                       # Número de cores utilizados no processo driver
spark_driver_memory=60g                     # Quantidade de memoria alocada no processo driver
spark_deploy_mode="client"                  # 'client' | 'cluster'. Define o responsável pelo processo driver.
                                            # 'client': spark-submit é responsável por rodar o driver, e só retornará quando o job spark tiver finalizado
                                            # 'cluster': cluster spawna o processo driver. Dessa forma, o spark-submit retorna imediatamente após o envio do job
                                            # enquanto o cluster processa o job. CUIDADO: caso esteja no modo 'cluster', o spark-submit retornará após o envio, e
                                            # se este script chegar ao fim, o slurm terminará o job antes do cluster terminar o processamento. 

function clean_job() {
  echo "Limpando ambiente..."  
  rm -rf "${local_job}"
}
trap clean_job EXIT HUP INT TERM ERR

set -eE

umask 077

echo "Copiando container..."
rclone copyto "${service}:${remote_sing}" "${local_sing}"

echo "Criando pastas temporárias..."
mkdir -p "${local_in}"
mkdir -p "${local_out}"
mkdir -p "${spark_conf_dir}"
mkdir -p "${spark_tmp_dir}"

echo "Escrevendo configuração do cluster Spark"

env=$spark_conf_dir/spark-env.sh
echo "SPARK_MASTER_HOST=$(hostname -f)" > $env
echo "SPARK_MASTER_PORT=7077" >> $env
echo "SPARK_WORKER_CORES=$spark_executor_cores" >> $env
echo "SPARK_WORKER_INSTANCES=$spark_executor_instances" >> $env
echo "SPARK_WORKER_MEMORY=$spark_executor_memory" >> $env
echo "SPARK_DRIVER_CORES=$spark_driver_cores" >> $env
echo "SPARK_DRIVER_MEMORY=$spark_driver_memory" >> $env


conf=$spark_conf_dir/spark-defaults.conf
# deployment 
echo "spark.submit.deployMode" client > $conf
echo "spark.master" $spark_master >> $conf
echo "spark.driver.cores" $spark_driver_cores >> $conf
echo "spark.driver.memory" $spark_driver_memory >> $conf
echo "spark.executor.cores" $spark_executor_cores >> $conf
echo "spark.executor.memory" $spark_executor_memory >> $conf
echo "spark.standalone.submit.waitAppCompletion true" >> $conf  # spark >= 3.0.0. Configuração faz com que o spark-submit espere completar o job em caso de
                                                                # deployMode 'cluster'

echo "Copiando input..."
rclone copy "${service}:${remote_in}/" "${local_in}/"


# Dados os arquivos de configurações gerados, o script start-worker.sh se encarregará de criar todos os workers (start-slave.sh quando spark < 3.0.0)
echo "Criando o cluster..."
echo "Criando o processo Master..."
singularity exec \
  --env SPARK_LOG_DIR=${local_job}/SparkLog,SPARK_MODE=master \
  --bind=/scratch:/scratch \
  --bind=/var/spool/slurm:/var/spool/slurm \
  --bind=$spark_conf_dir:/opt/bitnami/spark/conf \
  --bind=$spark_tmp_dir:/tmp \
  $local_sing start-master.sh
echo "Criando os processos Workers..."
singularity exec \
  --env SPARK_LOG_DIR=${local_job}/SparkLog,SPARK_MODE=worker,SPARK_WORKER_CORES=$spark_executor_cores,SPARK_WORKER_MEMORY=$spark_executor_memory,SPARK_WORKER_DIR=${local_job}/SparkWorker,SPARK_MASTER_URL=$spark_master \
  --bind=/scratch:/scratch \
  --bind=/var/spool/slurm:/var/spool/slurm \
  --bind=$spark_conf_dir:/opt/bitnami/spark/conf \
  --bind=$spark_tmp_dir:/tmp \
  $local_sing start-worker.sh $spark_master

echo "Enviando o job..."
# Para facilitar os experimentos, deixo a parametrização dos jobs para ser configurada na pasta de inputs
# dessa forma é possível construír um único arquivo de job configurando o ambiente e fazer um arquivo por experimento no drive
# é possível colocar o nome do arquivo aqui, no entanto optei por experimentos paths diferentes no drive
parameters_file = $(find $local_in -name "*.sh")
source $parameters_file
singularity exec \
    --bind=/scratch:/scratch \
    --bind=/var/spool/slurm:/var/spool/slurm \
    --bind=$spark_conf_dir:/opt/bitnami/spark/conf \
    --bind=$spark_tmp_dir:/tmp \
    $local_sing spark-submit \
        --master $spark_master \
        --executor-memory $spark_executor_memory \
        --driver-memory $spark_driver_memory \
        --driver-cores $spark_driver_cores \
        $local_in/$executable -M $mpts_list -d $local_in/$dataset -o $local_out/$output -p $partitions

echo "Enviando output..."
rclone move "${local_out}" "${service}:${remote_out}/"
rclone move "${SLURM_JOB_ID}.out" "${service}:${remote_out}/"
