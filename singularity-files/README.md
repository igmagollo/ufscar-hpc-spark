# Singularity files

Para criar as imagens singularity com spark, optei por utilizar a imagem de docker bitnami/spark. Esta é uma imagem pronta que já contém todas as dependencias padrões do spark. Além disso, existem tags para muitas versões de spark, basta consultar no dockerhub.

Caso seja necessário adicionar uma dependencia não padrão do spark, faça sua instalação no arquivo Singularity ou compile a dependencia inteira no `.jar` do job spark.

Como utilizamos apenas o `singularity exec` não é necessário o bloco de código `%runscript`, uma vez que esse bloco roda apenas na inicialização padrão do container (utilizando `singularity run`).
